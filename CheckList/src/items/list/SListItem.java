package items.list;

import processing.core.PApplet;
import processing.core.PFont;
import toxi.geom.Vec2D;
import items.gui.Button;
import items.gui.CheckBox;
import items.gui.HState;
import items.gui.State;
import items.gui.Text;

/**
 * 
 * Class that hold all the requirement GUI objects for a List Item
 * 
 * @author Stamatis
 *
 */
public class SListItem extends HState {

	private Button deleteButton;
	private CheckBox completeBox;
	private Text txtBox;

	private boolean toPrint = false;

	// These Parameters Control the Size of the Button in reference to the
	// getSizeY()
	private float p = .4f;

	public SListItem(State item, PFont txtFont, String txt) {
		super(item);


		State gap1 = new State(pa, new Vec2D(), 5, 2);

		completeBox = new CheckBox(item);
		completeBox.resize(p * getSizeY(), p * getSizeY());
		completeBox.setID("completeBox");

		txtBox = new Text(item, txtFont, txt);
		txtBox.resize( getSizeX(), getSizeY());
		txtBox.setID("txtBox");
		txtBox.setAdjustable(true);

		deleteButton = new Button(item);
		deleteButton.resize(p * getSizeY(), p * getSizeY());
		deleteButton.setID("deleteButton");

		add(gap1);
		add(completeBox);
		add(gap1);
		add(txtBox);
		add(gap1);
		add(deleteButton);
		add(gap1);
	}

	// public SListItem(PApplet pa, PFont txtFont, String txt, Vec2D pos,
	// float sizeX, float sizeY) {
	// super(new State(pa, pos, sizeX, sizeY));
	//
	// nudge = getSizeY() * p2;
	//
	// State gap1 = new State(pa, new Vec2D(), 5, 2);
	//
	// completeBox = new CheckBox(new State(pa, getPos().add(
	// new Vec2D(nudge, getSizeY() * p2)), sizeY * p, sizeY * p));
	//
	// completeBox.setID("completeBox");
	//
	// txtBox = new Text(new State(pa, getPos().add(
	// new Vec2D(completeBox.getPos().x + completeBox.getSizeX()
	// + nudge, 0)), getSizeX() - 2 * getSizeY(), getSizeY()),
	// txtFont, txt);
	// txtBox.setID("txtBox");
	// txtBox.setAdjustable(true);
	//
	// deleteButton = new Button(new State(pa, txtBox.getPos().add(
	// new Vec2D(txtBox.getSizeX() + nudge, getSizeY() * p2)),
	// getSizeY() * p, getSizeY() * p));
	// deleteButton.setID("deleteButton");
	//
	// add(gap1);
	// add(completeBox);
	// add(gap1);
	// add(txtBox);
	// add(gap1);
	// add(deleteButton);
	// add(gap1);
	// }

	/**
	 * @return Get the food Item
	 */
	public String getTXT() {
		return txtBox.getTXT();
	}

	/*
	 * Moves all Items to the Correct Location
	 */
	@Override
	public void move(Vec2D pos) {
		super.move(pos);
		adjust();
		arrayH();
		alignCentreV();
		// completeBox.move(getPos().add(new Vec2D(nudge, getSizeY() * p2)));
		// txtBox.move(getPos().add(new Vec2D(completeBox.getSizeX() + nudge,
		// 0)));
		// deleteButton.move(txtBox.getPos().add(
		// new Vec2D(txtBox.getSizeX() + nudge, getSizeY() * p2)));
	}

	public void update() {
		// super.update();
		adjust();
		arrayH();
		alignCentreV();
		txtBox.update();
		completeBox.update();
		deleteButton.update();
	}

	/**
	 * Draws all the Items on the window
	 * 
	 * @param finalAll
	 *            do not Draw delete Button, prepares for Print
	 */
	public void draw() {
		pa.pushStyle();
		pa.fill(238, 238, 255); // Background Colour
		pa.rect(getPos().x, getPos().y, getSizeX(), getSizeY());
		pa.fill(0); // Title Colour

		if (!isToPrint())
			deleteButton.drawCircle(true);
		if (completeBox.isChecked()) {
			pa.fill(200);
		} else {
			pa.fill(0);
		}

		completeBox.draw();
		txtBox.draw();

		pa.popStyle();
		// reset();
	}

	public void reset() {
		super.reset();
		txtBox.reset();
		completeBox.reset();
		deleteButton.reset();
	}

	/**
	 * @return true if delete Button is Pressed
	 */
	public boolean isDeleteClicked() {
		return deleteButton.onClick();
	}

	/**
	 * Mark Item as Checked
	 */
	public void check() {
		completeBox.check();
	}

	/**
	 * Mark Item as unChecked
	 */
	public void uncheck() {
		completeBox.uncheck();
	}

	/**
	 * Check if Item is Checked
	 * 
	 * @return true if Checked
	 */
	public boolean isChecked() {
		return completeBox.isChecked();
	}

	public Boolean isToPrint() {
		return toPrint;
	}

	public void setToPrint(Boolean toPrint) {
		this.toPrint = toPrint;
	}

	public void setToEdit(boolean toEdit) {
		txtBox.setToEdit(toEdit);
	}

	public boolean isToEdit() {
		return txtBox.isToEdit();
	}

}
