package items.list;

import java.util.ArrayList;

import items.gui.State;
import processing.core.PApplet;
import processing.core.PFont;
import toxi.geom.Vec2D;

public class SCartManager extends State {

	private ArrayList<SCartHandler> myCartHandler;
	private PFont txtFont;
	private State menu;

	public SCartManager(State myMenu, State items, PFont txtFont) {
		super(items);
		this.menu = myMenu;
		this.txtFont = txtFont;
		myCartHandler = new ArrayList<SCartHandler>();
	}

	public void run(int index) {
		update(index);
		draw(index);
		reset(index);
	}

	public void add() {
		myCartHandler.add(new SCartHandler(menu, this, "New Shopping Cart", txtFont));
	}
	
	public void add(SCartHandler cart) {
		myCartHandler.add(cart);
	}
	
	public void add(String txt) {
		myCartHandler.add(new SCartHandler(menu, this, txt, txtFont));		
	}

	public void update(int index) {
		get(index).update();
	}

	public void draw(int index) {
		get(index).draw();
	}

	public void reset(int index) {
		super.reset();
		get(index).reset();
	}

	public int size() {
		return myCartHandler.size();
	}

	public SCartHandler get(int index) {
		return myCartHandler.get(index);
	}

	public void open(String path) {
		String[] allData = pa.loadStrings(path);
		add(allData[0]);
		String[] butHeader = new String[allData.length - 1];
		System.arraycopy(allData, 1, butHeader, 0, butHeader.length);
		get(size() - 1).open(butHeader);
		
	}

	public void setToPrint(int index, boolean toPrint) {
		get(index).setToPrint(toPrint);
	}
}
