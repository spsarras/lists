package items.list;

import processing.core.PApplet;
import processing.core.PFont;
import items.gui.Button;
import items.gui.HState;
import items.gui.State;
import items.gui.Text;
import toxi.geom.Vec2D;

/**
 * This is Title for the Current Shopping Cart List and some UI elements to Help
 * with handling the Cart
 * 
 * @author Stamatis
 *
 */
public class SCartMenu extends HState {

	private Button saveButton;
	// private Button exportButton;
	private Button newCat;

	private Text title;

	private Boolean toPrint;

	// These Parameters Control the Size of the Button in reference to the
	// getSizeY()
	private float p = .75f;
	private float p2 = (1 - p) / 2f;

	public SCartMenu(State myMenu, PFont txtFont, String txt) {
		super(myMenu);

		State gap1 = new State(pa, new Vec2D(), 5, 2);
		title = new Text(new State(pa, getPos().add(p2 * getSizeY(),
				p2 * getSizeY()),
				getSizeX() - 2 * getSizeY() - p2 * getSizeY(), getSizeY() * p),
				txtFont, txt);
		title.setAdjustable(true);
		title.setID("title");

		newCat = new Button(new State(pa, title.getPos().add(
				new Vec2D(title.getSizeX() + p2 * getSizeY(), 0)), getSizeY()
				* p, getSizeY() * p));
		newCat.setID("newCat");

		saveButton = new Button(new State(pa, newCat.getPos().add(
				new Vec2D(getSizeY(), 0)), getSizeY() * p, getSizeY() * p));
		saveButton.setID("saveButton");

		add(gap1);
		add(title);
		add(gap1);
		add(newCat);
		add(gap1);
		add(saveButton);
		add(gap1);

		// exportButton = new Button(pa, saveButton.getPos().add(
		// new Vec2D(getSizeY(), 0)), getSizeY() * p);
	}

	public void move(Vec2D newPos) {
		super.move(newPos);
		adjust();
		arrayH();
		alignCentreV();

		// title.move(getPos().add(new Vec2D(3, p2 * getSizeY())));
		// newCat.move(title.getPos().add(
		// new Vec2D(title.getSizeX() + p2 * getSizeY(), 0)));
		// saveButton.move(newCat.getPos().add(new Vec2D(getSizeY(), 0)));
		// exportButton.move(saveButton.getPos().add(new Vec2D(getSizeY(), 0)));
	}

	public boolean isSaveButtonClicked() {
		return saveButton.onClick();
	}

	// public boolean isExportButtonClicked() {
	// return exportButton.onClick();
	// }

	public boolean isNewCatClicked() {
		return newCat.onClick();
	}

	public void update() {
		adjust();
		arrayH();
		alignCentreV();
		title.update();
		newCat.update();
		saveButton.update();
	}

	public String getTitle() {
		return title.getTXT();
	}

	public PFont getFont() {
		return title.getTxtFont();
	}

	public void draw() {
		pa.pushStyle();
		pa.fill(136, 85, 153);
		pa.rect(getPos().x, getPos().y, getSizeX(), getSizeY(), 0);
		pa.popStyle();

		// super.drawBB();
		title.draw();
		saveButton.drawDisk(true);
		// exportButton.drawExport();
		newCat.drawPlus(true);
	}

	public void reset() {
		title.reset();
		saveButton.reset();
		// exportButton.reset();
		newCat.reset();
	}

	public boolean isToPrint() {
		return toPrint;
	}

	public void setToPrint(boolean toPrint) {
		this.toPrint = toPrint;
	}

}
