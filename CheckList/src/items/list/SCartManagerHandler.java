package items.list;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PFont;
import toxi.geom.Vec2D;
import items.gui.State;

public class SCartManagerHandler extends State  {


	private SCartManager myCartManagers;
	private SCartHandler myCartToPrint;
	private SCartManagerMenu menu;
	private int index = 0;

	public SCartManagerHandler(State myMenu, State items, PFont txtFont) {
		super(myMenu);
		menu = new SCartManagerMenu(myMenu, txtFont, "Shopping Cart Editor");
		
		State itemMenu = new State(items.getPA(), items.getPos(), items.getSizeX(), this.getSizeY());
		
		myCartManagers = new SCartManager(itemMenu, items, txtFont);
//		myCartManagers.get(0).getMenu().setSizeY(menu.getSizeY());
		myCartManagers.open("Food.txt");
		updateExportCart();
	}

//	public SCartManagerHandler(PApplet pa, PFont txtFont, Vec2D pos,
//			float sizeX, float sizeY) {
//		super(pa, pos, sizeX, sizeY);
//		menu = new SCartManagerMenu(pa, txtFont, "Shopping Cart Editor",
//				getPos(), getSizeX(), getSizeY());
//		myCartManagers = new SCartManager(pa, txtFont, getPos().add(
//				new Vec2D(0, getSizeY())), getSizeX(), getSizeY());
//		myCartManagers.add();
//	}

	public void updateExportCart() {
		myCartToPrint = new SCartHandler(myCartManagers.get(index));
	}

	/**
	 * Open up a new Cart
	 */
	public void openClicked() {
		if (menu.isOpenClicked()) {
			final JFileChooser fc = new JFileChooser();

			// Header
			fc.setDialogTitle("Open a Cart");

			File startFile = new File(System.getProperty("user.dir")); // Get
																		// the
																		// current
																		// directory
			// System.out.println(startFile.getPath());

			// Find System Root
			while (!FileSystemView.getFileSystemView().isFileSystemRoot(
					startFile)) {
				startFile = startFile.getParentFile();
			}
			// Changed the next line
			fc.setCurrentDirectory(fc.getFileSystemView().getParentDirectory(
					startFile));

			// add filters
			FileNameExtensionFilter txtFilter = new FileNameExtensionFilter(
					"Text files (*.txt)", "txt");
			fc.addChoosableFileFilter(txtFilter);
			fc.setFileFilter(txtFilter);

			fc.showOpenDialog(null);

			if (fc.getSelectedFile() != null) {
				String path = fc.getSelectedFile().getPath();
				System.out.println(path);
				myCartManagers.open(path);
				index = myCartManagers.size() - 1;
			}
		}

	}

	/**
	 * Behaviour of the NextButton, goes directly to the next Cart
	 */
	public void nextClicked() {
		if (menu.isNextClicked()) {
			index++;
			if (index == myCartManagers.size())
				index = 0;

		}
	}

	/**
	 * Behaviour of the new Button, goes directly to the new Cart
	 */
	public void newClicked() {
		if (menu.isNewClicked()) {
			myCartManagers.add();
			myCartManagers.get(myCartManagers.size()-1).setSizeY(menu.getSizeY());
			index = myCartManagers.size() - 1;
		}
	}

	/**
	 * Method that wraps all the functionality
	 */
	public void run() {

		update();

		if (menu.isExportClicked()) {
			setToPrint(true);
			pa.beginRecord(PConstants.PDF, "frame-####.pdf");
		}

		draw();

		if (menu.isExportClicked())
			pa.endRecord();

		reset();

	}

	/**
	 * Method with the behaviours
	 */
	public void update() {
		openClicked();
		nextClicked();
		newClicked();
		menu.update();
		updateExportCart();
		
		if (menu.isExportClicked())
			myCartToPrint.update();
		else
			myCartManagers.update(index);
		
	}


	/**
	 * Drawing the Objects
	 */
	public void draw() {
		pa.background(255);

		menu.draw();
		if (menu.isExportClicked())
			myCartToPrint.draw();
		else
			myCartManagers.draw(index);

	}

	public void reset() {
		super.reset();
		menu.reset();
		myCartManagers.reset(index);
		setToPrint(false);
	}

	public void setToPrint(boolean toPrint) {
		myCartToPrint.setToPrint(toPrint);
//		myCartManagers.setToPrint(index, toPrint);
	}

	
	private void writeObject(java.io.ObjectOutputStream out) throws IOException {

	}

	private void readObject(java.io.ObjectInputStream in) throws IOException,
			ClassNotFoundException {

	}
	
}
