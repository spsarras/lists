package items.list;

import items.gui.State;
import processing.core.PFont;
import toxi.geom.Vec2D;
import processing.core.PApplet;

public class SListHandler extends State {

	private SList listOfItems;
	private SListMenu menu;

	public SListHandler(State myMenu, State items, PFont txtFont, String txtHeader) {
		super(myMenu);
		menu = new SListMenu(myMenu, txtFont, txtHeader);
		listOfItems = new SList(items, txtFont);
	}

	/**
	 * @param finalAll
	 *            if final it will not draw the delete Icon on each Item on the
	 *            List and Remove the Button on the Header
	 */
	public void draw() {
		// drawBox();
		listOfItems.draw();
		menu.draw();
	}

	/**
	 * @return get a String on the Form: fruits, orange, apple, pear
	 */
	public String save() {
		return menu.getTXT() + "," + listOfItems.save();
	}

	/**
	 * @return get the Header Title
	 */
	public String getTXT() {
		return menu.getTXT();
	}

	/**
	 * Draw a Bold Box Around the List
	 */
	public void drawBox() {
		pa.pushStyle();
		pa.fill(255); // Category Color
		// noFill();
		pa.stroke(0);
		pa.strokeWeight(1.5f);
		pa.rect(getPos().x, getPos().y, getSizeX(), getSizeY());
		pa.popStyle();
	}

	/**
	 * Add an Item on the List with the a default Text
	 */
	public void add() {
		listOfItems.add();
		// listOfItems.arrange(); // delete

	}

	public void add(String name) {
		listOfItems.add(name);
	}

	public void input(String[] name) {
		for (int i = 0; i < name.length; i++) {
			add(name[i]);
		}
	}

	/**
	 * Update The Objects' functionality and some of the Class functionality
	 */
	public void update() {
		updateSize();
		updateStatus();
		// edit();
		deleteClicked();
		addClicked();
		// reset();
		if (!(isToEdit() == size()))
			listOfItems.update();
		if (!(isToEdit() >= 0))
			menu.update();

	}

	public void edit() {
		// TODO implement this operation
		throw new UnsupportedOperationException("not implemented");
	}

	// void deselect() {
	// editing = false;
	// index = -1;
	// }

	/**
	 * Delete one Item by Clicking
	 */
	public void deleteClicked() {
		int indexToRemove = -1;
		for (int i = 0; i < listOfItems.size(); i++) {
			if (listOfItems.get(i).isDeleteClicked()) {
				// deselect();
				indexToRemove = i;
				// editing = false;
			}
		}

		if (indexToRemove >= 0) {
			listOfItems.remove(indexToRemove);
		}
		// listOfItems.arrange(); // delete
	}

	/**
	 * @return
	 */
	public boolean isDeleteClicked() {
		return menu.deleteClicked();
	}

	/**
	 * Add one Item if Add on The Header is Clicked
	 */
	public void addClicked() {
		if (menu.isAddClicked()) {
			add();
		}
	}

	public void updateStatus() {
		if (menu.isCheckBoxClicked()) {
			if (!menu.isChecked()) {
				listOfItems.checkAll();
			} else {
				listOfItems.unCheckAll();
			}
		}
	}

	public boolean allChecked() {
		// TODO implement this operation
		throw new UnsupportedOperationException("not implemented");
	}

	public void updateSize() {
		setSizeY(listOfItems.size() * listOfItems.getSizeY() + menu.getSizeY());
	}

	public void move(Vec2D newPos) {
		super.move(newPos);
		menu.move(newPos);
		listOfItems.move(newPos.add(0, menu.getSizeY()));
	}

	@Override
	public void reset() {
		super.reset();
		menu.reset();
		listOfItems.reset();
	}

	public PFont getTXTFont() {
		return listOfItems.getTXTFont();
	}

	public int size() {
		return listOfItems.size();
	}

	public SListItem get(int index) {
		return listOfItems.get(index);
	}

	public void add(String[] items) {
		for (int i = 0; i < items.length; i++) {
			add(items[i]);
		}
	}

	public Vec2D getLastPos() {
		Vec2D pos = listOfItems.getLastPos();
		if (pos == null)
			return menu.getLastPos();
		else
			return listOfItems.getLastPos();
	}

	public void arrange(Vec2D entry, float minY, float maxY) {
		move(entry);
		listOfItems.arrange(menu.getLastPos(), minY, maxY);
	}

	public void setToPrint(boolean toPrint) {
		listOfItems.setToPrint(toPrint);
		menu.setToPrint(toPrint);
	}

	public void run() {
		update();
		draw();
		reset();
	}

	public int isToEdit() {
		if (menu.isToEdit())
			return size();
		return listOfItems.isToEdit();
	}

	public boolean isFull() {
		for (int i = 0; i < size(); i++) {
			if (!get(i).isChecked())
				return false;
		}
		return true;
	}

}
