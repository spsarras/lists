package items.list;

import java.util.ArrayList;

import items.gui.State;
import toxi.geom.Vec2D;
import processing.core.PFont;
import processing.core.PApplet;

public class SCart extends State {

	private ArrayList<SListHandler> myLists;
	private PFont txtFont;
	private State menu;

	SCart(State items, State menu, PFont txtFont) {
		super(items);
		this.menu = menu;
		this.txtFont = txtFont;
		myLists = new ArrayList<SListHandler>();
	}

	public void arrange(float minY, float maxY) {
		Vec2D entryPoint = getPos();
		entryPoint.y = minY;
		for (int i = 0; i < size(); i++) {
			if (getSizeY() + entryPoint.y > maxY) {
				entryPoint = entryPoint
						.add(new Vec2D(get(i - 1).getSizeX(), 0));
				entryPoint.y = minY;
			}

			get(i).arrange(entryPoint.copy(), minY, maxY);
			entryPoint = get(i).getLastPos();
		}
	}

	/**
	 * @return get the String to save of all the categories
	 */
	public String[] save() {
		String[] toSave = new String[size()];
		for (int i = 0; i < size(); i++) {
			toSave[i] = get(i).save();
		}
		return toSave;
	}

	/**
	 * 
	 */
	public void update() {
		arrange(this.getPos().y, pa.height);
		int index = isToEdit();
		if (index < 0) {
			for (int i = 0; i < size(); i++) {
				get(i).update();
			}
		} else {
			get(index).update();
		}
	}

	public void reset() {
		super.reset();
		for (int i = 0; i < size(); i++) {
			get(i).reset();
		}
	}

	public void draw() {
		for (int i = 0; i < size(); i++) {
			get(i).draw();
		}
	}

	// //////ArrayList/////////
	public int size() {
		return myLists.size();
	}

	public void add(SListHandler newList) {
		myLists.add(newList);
	}

	public void add() {
		myLists.add(new SListHandler(menu, this, txtFont, ""));
	}

	public void add(String name) {
		myLists.add(new SListHandler(menu, this, txtFont, name));
	}

	public void add(String name, String[] items) {
		add(name);
		get(size() - 1).add(items);
	}

	public void remove(int index) {
		myLists.remove(index);
	}

	public SListHandler get(int index) {
		return myLists.get(index);
	}

	public PFont getTXTFont() {
		return txtFont;
	}

	public void open(String[] butHeader) {
		for (int i = 0; i < butHeader.length; i++) {
			String[] headerItems = PApplet.split(butHeader[i], ',');
			String[] items = new String[headerItems.length - 1];
			System.arraycopy(headerItems, 1, items, 0, items.length);
			add(headerItems[0], items);
		}
	}

	public void setToPrint(boolean toPrint) {
		for (int i = 0; i < size(); i++) {
			get(i).setToPrint(toPrint);
		}
	}

	public int isToEdit() {
		int index = -1;
		for (int i = 0; i < size(); i++) {
			if (get(i).isToEdit() >= 0)
				index = i;
		}
		return index;
	}

}
