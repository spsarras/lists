package items.list;

import items.gui.Button;
import items.gui.CheckBox;
import items.gui.HState;
import items.gui.State;
import items.gui.Text;
import processing.core.PApplet;
import processing.core.PFont;
import toxi.geom.Vec2D;

/**
 * A Class to Hold the Header of each Shopping List with Buttons to control the
 * Size of the List
 * 
 * @author Stamatis
 * 
 */
public class SListMenu extends HState {

	private CheckBox statusBox;
	private Button addButton;
	private Button deleteButton;
	private Text txtBox;
	private boolean toPrint;

	// These Parameters Control the Size of the Button in reference to the
	// getSizeY()
	private float p = .75f;
	private float p2 = (1 - p) / 2f;

	public SListMenu(State menu, PFont txtFont, String txt) {
		super(menu);

		State gap1 = new State(pa, new Vec2D(), 5, 2);
		txtBox = new Text(menu, txtFont, txt);
		txtBox.setAdjustable(true);
		txtBox.setID("txtBox");

		statusBox = new CheckBox(menu);
		statusBox.resize(getSizeY() * p, getSizeY() * p);
		statusBox.setID("statusBox");

		addButton = new Button(menu);
		addButton.resize(getSizeY() * p, getSizeY() * p);
		addButton.setID("addButton");

		deleteButton = new Button(menu);
		deleteButton.resize(getSizeY() * p, getSizeY() * p);
		deleteButton.setID("deleteButton");

		add(gap1);
		add(txtBox);
		add(gap1);
		add(statusBox);
		add(gap1);
		add(addButton);
		add(gap1);
		add(deleteButton);
		add(gap1);
	}

	// public SListMenu(PApplet pa, PFont txtFont, String txt, Vec2D pos,
	// float sizeX, float sizeY) {
	// super(new State(pa, pos, sizeX, sizeY));
	//
	// State gap1 = new State(pa, new Vec2D(), 5, 2);
	// txtBox = new Text(new State(pa, getPos().add(
	// new Vec2D(0, getSizeY() * p2)), sizeX - 3 * getSizeY(),
	// getSizeY() * p), txtFont, txt.toUpperCase());
	// txtBox.setAdjustable(true);
	// txtBox.setID("txtBox");
	//
	// statusBox = new CheckBox(new State(pa, getPos().add(
	// new Vec2D(txtBox.getSizeX(), getSizeY() * p2)), getSizeY() * p,
	// getSizeY() * p));
	// statusBox.setID("statusBox");
	//
	// addButton = new Button(new State(pa, getPos().add(
	// new Vec2D(statusBox.getPos().x + getSizeY(), getSizeY() * p2)),
	// getSizeY() * p, getSizeY() * p));
	// addButton.setID("addButton");
	//
	// deleteButton = new Button(new State(pa, getPos().add(
	// new Vec2D(addButton.getPos().x + getSizeY(), getSizeY() * p2)),
	// getSizeY() * p, getSizeY() * p));
	// deleteButton.setID("deleteButton");
	//
	// add(gap1);
	// add(txtBox);
	// add(gap1);
	// add(statusBox);
	// add(gap1);
	// add(addButton);
	// add(gap1);
	// add(deleteButton);
	// add(gap1);
	//
	// }

	public String getTXT() {
		return txtBox.getTXT();
	}

	/*
	 * Moves all the elements, keeping correct distances
	 * 
	 * @see items.interaction.State#move(toxi.geom.Vec2D)
	 */
	@Override
	public void move(Vec2D newPos) {
		super.move(newPos);
		adjust();
		arrayH();
		alignCentreV();
	}

	/**
	 * @param finalAll
	 *            if this is for final Print, will rearrange the buttons
	 */
	public void arrange(boolean finalAll) {
		if (finalAll) {
			
			this.remove("statusBox");
			this.remove("addButton");
			
//			statusBox.move(txtBox.getPos().add(
//					txtBox.getSizeX() + 2 * getSizeY(), 0));
		} else {
//			statusBox
//					.move(txtBox.getPos().add(new Vec2D(txtBox.getSizeX(), 0)));
		}
	}

	/**
	 * This Runs the functionality of each Element
	 */
	public void update() {
		arrange(isToPrint());

		adjust();
		arrayH();
		alignCentreV();

		statusBox.update();
		txtBox.seTXT(txtBox.getTXT().toUpperCase());
		txtBox.update();
	}

	/**
	 * @param finalAll
	 *            if True will draw only the Tick Box and the Title, otherwise
	 *            will print all the Buttons with the Title
	 */
	public void draw() {
		pa.pushStyle();
		pa.fill(187, 204, 170); // Background Colour
		pa.rect(getPos().x, getPos().y, getSizeX(), getSizeY());
		pa.fill(0); // Title Colour
		pa.popStyle();


		statusBox.draw();
		txtBox.draw();

		if (!isToPrint()) {
			deleteButton.drawCircle(true);
			addButton.drawPlus(true);
		}
	}

	/**
	 * Used in events that you want to accure after it is pressed
	 * 
	 * @return true if the Check box is clicked
	 */
	public boolean isCheckBoxClicked() {
		return statusBox.onClick();
	}

	/**
	 * @return if the AddButton is Clicked
	 */
	public boolean isAddClicked() {
		return addButton.onClick();
	}

	/**
	 * @return if the Tick Box is Checked
	 */
	public boolean isChecked() {
		return statusBox.isChecked();
	}

	/**
	 * @return if the Header is Clicked
	 */
	public boolean isClicked() {
		return this.onClick();
	}

	/**
	 * @return id delete Button is Clicked
	 */
	public boolean deleteClicked() {
		return deleteButton.onClick();
	}

	@Override
	public void reset() {
		super.reset();
		addButton.reset();
		deleteButton.reset();
		statusBox.reset();
		txtBox.reset();
	}

	public Vec2D getLastPos() {
		return getPos().add(0, getSizeY());
	}

	public boolean isToPrint() {
		return toPrint;
	}

	public void setToPrint(boolean toPrint) {
		this.toPrint = toPrint;
	}

	public boolean isToEdit() {
		return txtBox.isToEdit();
	}
}
