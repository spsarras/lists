package items.list;

import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PImage;
import toxi.geom.Vec2D;
import items.gui.Button;
import items.gui.HState;
import items.gui.State;
import items.gui.Text;

public class SCartManagerMenu extends HState {

	private Button nextButton;
	private Button newButton;
	private Button openButton;
	private Button exportButton;

	private Text title;
	private transient PImage img;

	// These Parameters Control the Size of the Button in reference to the
	// getSizeY()
	private float p = .75f;
	private float p2 = (1 - p) / 2f;

	public SCartManagerMenu(State myMenu, PFont txtFont, String txt) {
		super(myMenu);

		State gap1 = new State(pa, new Vec2D(), 5, 2);

		title = new Text(new State(pa, getPos().add(p2 * getSizeY(),
				p2 * getSizeY()),
				getSizeX() - 4 * getSizeY() - p2 * getSizeY(), getSizeY() * p),
				txtFont, txt);
		title.setID("title");
		title.setAdjustable(true);

		exportButton = new Button(new State(pa, title.getPos().add(
				title.getSizeX() + p2 * getSizeY(), 0), getSizeY() * p,
				getSizeY() * p));
		exportButton.setID("exportButton");

		newButton = new Button(new State(pa, exportButton.getPos().add(
				getSizeY(), 0), getSizeY() * p, getSizeY() * p));
		newButton.setID("newButton");

		openButton = new Button(new State(pa, newButton.getPos().add(
				getSizeY(), 0), getSizeY() * p, getSizeY() * p));
		openButton.setID("openButton");

		nextButton = new Button(new State(pa, openButton.getPos().add(
				getSizeY(), 0), getSizeY() * p, getSizeY() * p));
		nextButton.setID("nextButton");

		img = pa.loadImage("openIcon.png");

		add(gap1);
		add(title);
		add(gap1);
		add(exportButton);
		add(gap1);
		add(newButton);
		add(gap1);
		add(openButton);
		add(gap1);
		add(nextButton);
		add(gap1);

	}

	public void move(Vec2D newPos) {
		super.move(newPos);
		adjust();
		arrayH();
		alignCentreV();
		// title.move(getPos().add(p2 * getSizeY(), p2 * getSizeY()));
		// exportButton.move(title.getPos().add(title.getSizeX(), 0));
		// newButton.move(exportButton.getPos().add(getSizeY(), 0));
		// openButton.move(newButton.getPos().add(getSizeY(), 0));
		// nextButton.move(openButton.getPos().add(getSizeY(), 0));
	}

	/**
	 * For Interacting with the Menu
	 * 
	 * @return nextButton is pressed
	 */
	public boolean isNextClicked() {
		return nextButton.onClick();
	}

	/**
	 * For Interacting with the Menu
	 * 
	 * @return openButton is pressed
	 */
	public boolean isOpenClicked() {
		return openButton.onClick();
	}

	/**
	 * For Interacting with the Menu
	 * 
	 * @return newButton is pressed
	 */
	public boolean isNewClicked() {
		return newButton.onClick();
	}

	/**
	 * For Interacting with the Menu
	 * 
	 * @return newButton is pressed
	 */
	public boolean isExportClicked() {
		return exportButton.onClick();
	}

	/**
	 * Overall routing summing up update(), draw(), reset()
	 */
	public void run() {
		update();
		draw();
		reset();
	}

	/**
	 * Update the Behaviours
	 */
	public void update() {
		title.update();
		nextButton.update();
		newButton.update();
		openButton.update();
		exportButton.update();
		adjust();
		arrayH();
		alignCentreV();
	}

	/**
	 * Draw all the Buttons/textField
	 */
	public void draw() {
		pa.pushStyle();
		pa.fill(235, 207, 124);
		pa.rect(getPos().x, getPos().y, getSizeX(), getSizeY(), 0);
		pa.popStyle();

		// title.draw();
		title.draw();
		nextButton.drawNext();
		newButton.drawPlus(true);
		openButton.drawImage(img);
		exportButton.drawExport();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see items.interaction.State#reset()
	 */
	public void reset() {
		title.reset();
		nextButton.reset();
		newButton.reset();
		openButton.reset();
		exportButton.reset();
	}

}
