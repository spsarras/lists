package items.list;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import items.gui.State;
import processing.core.PApplet;
import processing.core.PFont;
import toxi.geom.Vec2D;

public class SCartHandler extends State {

	private SCart myCart;
	private SCartMenu menu;

	public SCartHandler(State myMenu, State items, String txt, PFont txtFont) {
		super(myMenu);
		menu = new SCartMenu(myMenu, txtFont, txt);
		myCart = new SCart(myMenu, items, txtFont);
		// myCart.add();
	}

	public void updatePos() {
		myCart.move(menu.getPos().add(new Vec2D(0, menu.getSizeY())));
	}

	public SCartHandler(SCartHandler cartToCopy) {
		super(cartToCopy.pa, cartToCopy.getPos(), cartToCopy.getSizeX(),
				cartToCopy.getSizeY());
		menu = new SCartMenu(cartToCopy.getMenu(), cartToCopy.getMenu()
				.getFont(), cartToCopy.getMenu().getTitle());
		myCart = new SCart(cartToCopy.getMenu(), cartToCopy.getCart(),
				cartToCopy.getMenu().getFont());

		for (int i = 0; i < cartToCopy.size(); i++) {
			SListHandler myList = cartToCopy.get(i);

			if (!myList.isFull()) {

				myCart.add(myList.getTXT());
				for (int j = 0; j < myList.size(); j++) {
					SListItem myItem = myList.get(j);

					if (!myItem.isChecked())
						myCart.get(size() - 1).add(myItem.getTXT());

				}
			}
		}
	}

	private SCart getCart() {
		return myCart;
	}

	/**
	 * Inputs the Items found on the textFIle TODO fragment it inside the
	 * subclasses
	 * 
	 * @param path
	 */
	public void input(String path) {
		String[] allData = pa.loadStrings(path);
		for (int i = 0; i < allData.length; i++) {
			String[] category = PApplet.split(allData[i], ',');

			SListHandler newList = new SListHandler(menu, myCart,
					myCart.getTXTFont(), category[0]);

			for (int j = 1; j < category.length; j++) {
				newList.add(category[j]);
			}
			myCart.add(newList);
		}
	}

	/**
	 * @param cartToSave
	 * @param path
	 */
	// public void save(SCartHandler cartToSave, String path) {
	// String[] data = new String[cartToSave.myCart.size()];
	// for (int i = 0; i < data.length; i++) {
	// // ArrayList<listItem> toCheck = cartToSave.get(i).listItems;
	// data[i] = cartToSave.get(i).getTXT() + ",";
	// for (int j = 0; j < cartToSave.get(i).size(); j++) {
	// if (j != cartToSave.get(i).size() - 1)
	// data[i] += (cartToSave.get(i).get(j).getTXT() + ",");
	// else
	// data[i] += (cartToSave.get(i).get(j).getTXT());
	// }
	// }
	// pa.saveStrings(path, data);
	// }

	public void save(String path) {
		// System.arraycopy(src, srcPos, dest, destPos, length);
		pa.saveStrings(path, myCart.save());
	}

	public void exportClicked() {
		if (menu.isSaveButtonClicked()) {
			final JFileChooser fc = new JFileChooser();

			// Header
			fc.setDialogTitle("Export Just Cart to .txt");

			// Set Current View
			File startFile = new File(System.getProperty("user.dir")); // Get
																		// the
																		// current
																		// directory
			System.out.println(startFile.getPath());

			// Find System Root
			while (!FileSystemView.getFileSystemView().isFileSystemRoot(
					startFile)) {
				startFile = startFile.getParentFile();
			}
			// Changed the next line
			fc.setCurrentDirectory(fc.getFileSystemView().getParentDirectory(
					startFile));

			// add filters
			FileNameExtensionFilter txtFilter = new FileNameExtensionFilter(
					"Text files (*.txt)", "txt");
			fc.addChoosableFileFilter(txtFilter);
			fc.setFileFilter(txtFilter);

			fc.showSaveDialog(null);

			String path = fc.getSelectedFile().getPath();

			if (path != null)
				save(path);
		}
	}

	public SListHandler get(int index) {
		return myCart.get(index);
	}

	public void update() {
		updatePos();
		// deselect();
		exportClicked();
		deleteClicked();
		addClicked();
		menu.update();
		myCart.update();
	}

	public void draw() {
		menu.draw();
		myCart.draw();
	}

	public void reset() {
		menu.reset();
		myCart.reset();
	}

	public void deselect() {
		// TODO implement this operation
		throw new UnsupportedOperationException("not implemented");
	}

	public int size() {
		return myCart.size();
	}

	public void addClicked() {
		if (menu.isNewCatClicked())
			add();
	}

	/**
	 * 
	 */
	public void deleteClicked() {
		for (int i = 0; i < myCart.size(); i++) {
			if (myCart.get(i).isDeleteClicked())
				myCart.remove(i);
		}
	}

	public void add() {
		myCart.add();
	}

	public void open(String[] butHeader) {
		myCart.open(butHeader);
	}

	public void setToPrint(boolean isPrint) {
		myCart.setToPrint(isPrint);
		menu.setToPrint(isPrint);
	}

	public SCartMenu getMenu() {
		return menu;
	}

}
