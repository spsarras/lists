package items.list;

import items.gui.State;

import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PFont;
import toxi.geom.Vec2D;

public class SList extends State {

	private ArrayList<SListItem> myList;
	private PFont txtFont;

	private boolean toPrint;

	public SList(State item, PFont txtFont) {
		super(item);
		this.txtFont = txtFont;
		myList = new ArrayList<SListItem>();
	}

	@Override
	public void move(Vec2D newPos) {
		super.move(newPos);
		for (int i = 0; i < myList.size(); i++) {
			get(i).move(get(i).getPos().add(newPos));
		}
	}

	public void arrange() {
		move(getPos());
	}

	public void arrange(Vec2D entry, float minY, float maxY) {

		Vec2D entryPoint = getPos().copy();
		entryPoint.y = entry.y;
		for (int i = 0; i < size(); i++) {
			if (get(i).getSizeY() + entryPoint.y > maxY) {
				entryPoint = entryPoint.add(new Vec2D(getSizeX(), 0));
				entryPoint.y = minY;
			}
			get(i).move(entryPoint.copy());
			entryPoint = entryPoint.add(0, get(i).getSizeY());
		}
	}

	/**
	 * Return the Position of the last Item on the List. Used to Get the new
	 * placeholder of the next Item
	 * 
	 * @return
	 */
	public Vec2D getLastPos() {
		SListItem last;
		if (size() > 0)
			last = get(size() - 1);
		else
			return null;

		Vec2D pos = last.getPos().copy();
		return pos.add(0, last.getSizeY());

	}

	/**
	 * Add a Simple Item at the end of the List
	 */
	public void add() {
		myList.add(new SListItem(new State(pa, new Vec2D(getPos().x, getPos().y
				+ myList.size() * getSizeY()), getSizeX(), getSizeY()),
				txtFont, ""));
	}

	/**
	 * Add a Simple Item at the end of the List, used when import
	 * 
	 * @param name
	 */
	public void add(String name) {
		myList.add(new SListItem(new State(pa, new Vec2D(getPos().x, getPos().y
				+ myList.size() * getSizeY()), getSizeX(), getSizeY()),
				txtFont, name));
	}

	public void input(String[] name) {
		for (int i = 0; i < name.length; i++) {
			add(name[i]);
		}
	}

	/**
	 * @param finalAll
	 *            if final it will not draw the delete Icon on each Item on the
	 *            List
	 */
	public void draw() {
		for (int i = 0; i < myList.size(); i++) {
			myList.get(i).draw();
		}
	}

	/**
	 * @return the number of items on myList
	 */
	public int size() {
		return myList.size();
	}

	/**
	 * Tick all the Boxes
	 */
	public void checkAll() {
		for (int i = 0; i < size(); i++) {
			myList.get(i).check();
		}
	}

	/**
	 * Untick all the Boxes
	 */
	public void unCheckAll() {
		for (int i = 0; i < size(); i++) {
			myList.get(i).uncheck();
		}
	}

	public PFont getTXTFont() {
		return txtFont;
	}

	public void reset() {
		super.reset();
		for (int i = 0; i < size(); i++) {
			myList.get(i).reset();
		}
	}

	public void update() {
		int index = isToEdit();
		if (index == -1) {
			for (int i = 0; i < size(); i++) {
				myList.get(i).update();
			}
		} else
			myList.get(index).update();
	}

	/**
	 * Implementing Basic Methods of ArrayList
	 * 
	 * @param i
	 * @return
	 */
	public SListItem get(int i) {
		return myList.get(i);
	}

	public void remove(int i) {
		myList.remove(i);
	}

	/**
	 * @return Get A String like that: Apple, Pear, Banana
	 */
	public String save() {

		String toSave = "";

		for (int i = 0; i < size(); i++) {
			if (i == size() - 1)
				toSave += myList.get(i).getTXT();
			else
				toSave += myList.get(i).getTXT() + ",";
		}

		return toSave;
	}

	public boolean isToPrint() {
		return toPrint;
	}

	public void setToPrint(boolean toPrint) {
		for (int i = 0; i < size(); i++) {
			myList.get(i).setToPrint(toPrint);
		}
		this.toPrint = toPrint;
	}

	public int isToEdit() {
		int index = -1;
		for (int i = 0; i < size(); i++) {
			if (get(i).isToEdit())
				return index = i;
		}
		return index;
	}

}
