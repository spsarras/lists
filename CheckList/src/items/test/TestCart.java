package items.test;

import items.gui.State;
import items.list.SCartHandler;
import items.list.SCartManager;
import items.list.SCartManagerHandler;
import items.list.SCartManagerMenu;
import items.list.SCartMenu;
import processing.core.PApplet;
import processing.core.PFont;
import toxi.geom.Vec2D;

public class TestCart extends PApplet {
	
	PFont pf;
	SCartManagerHandler myCartManager;

	public void setup() {
		size(640, 480);
		pf = createFont("Times New Roman Bold Italic", 16);
		State myState = new State(this, new Vec2D(), 150, 30);
		myCartManager = new SCartManagerHandler(myState, myState, pf);
	}
	
	public void draw() {
		
		background(255);
		
		myCartManager.run();		
		
	}
	
}
