package items.test;

import java.io.File;

import javax.swing.JFileChooser;
import processing.core.PApplet;

/**
 * @author Stamatis
 * A Simple Example showing how to lunch a file chooser
 *
 */
public class OpenFile extends PApplet{

//	//Create a file chooser
	final JFileChooser fc = new JFileChooser();
	

    
    
    
	public void setup() {
		fc.setCurrentDirectory(new File("c:/"));
		fc.setDialogTitle("Open Your File");
	}
	
	public void draw() {
		
		System.out.println(frameCount);
	}
	
	public void mousePressed() {
		if(mouseButton == LEFT) {
			int returnVal = fc.showOpenDialog(null);	
//		    int rVal = fc.showSaveDialog(null);
			System.out.println(fc.getSelectedFile());			
		}
	}
	
}
