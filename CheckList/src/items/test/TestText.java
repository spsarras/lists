package items.test;

import items.gui.State;
import items.gui.Text;
import items.list.SListHandler;
import items.list.SListItem;
import items.list.SListMenu;
import processing.core.PApplet;
import processing.core.PFont;
import toxi.geom.Vec2D;

public class TestText extends PApplet {

	Text myText;
	PFont pf;
	SListMenu myTitle;
	SListItem myListItem;

	SListHandler myListHandler;

	public void setup() {
		size(250, 250);
		pf = createFont("Times New Roman Bold Italic", 16);
		myText = new Text(new State(this, new Vec2D(0, 0), 200, 20), pf,
				"start");
		myTitle = new SListMenu(new State(this, new Vec2D(0, 40), 200, 30), pf,
				"My Title");
		myListItem = new SListItem(new State(this, new Vec2D(0, 80), 200, 30),
				pf, "item");

		myListHandler = new SListHandler(new State(this, new Vec2D(0, 120),
				200, 30), new State(this, new Vec2D(0, 120), 100, 30), pf,
				"New Cat");
		myListHandler.add();
		myListHandler.add();
		myListHandler.add();
	}

	public void draw() {
		background(255);

		myText.update();
		myText.draw();
		myText.reset();

		myTitle.update();
		myTitle.draw();
		myTitle.reset();

		// myListHandler.move(new Vec2D(mouseX, mouseY));
		myListHandler.update();
		myListHandler.draw();
		myListHandler.reset();

	}

	boolean display = false;

	public void keyPressed() {
		if (key == ' ') {
			display = !display;
		}
	}
}
