package items.test;

import items.gui.State;
import processing.core.PApplet;
import toxi.geom.Vec2D;

public class TestState extends PApplet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1010627138180682438L;
	State myState;
	State myState2;
	
	public void setup() {
		size(40, 40);
		myState = new State(new State(this, new Vec2D(0, 0), 20, 20));
		myState2 = new State(this, new Vec2D(20, 20), 20, 20);
	}

	public void draw() {
		background(255);
		myState.drawBB();
		myState.onClick();
		myState.reset();

		myState2.drawBB();
		myState2.onClick();
		myState2.reset();
	}
	
}
