package items.test;

import items.gui.State;
import items.list.SListHandler;
import processing.core.PApplet;
import processing.core.PFont;
import toxi.geom.Vec2D;

public class ArrangeTest extends PApplet {

	SListHandler myList;
	PFont pf;

	public void setup() {
		size(600, 400);

		pf = createFont("Times New Roman Bold Italic", 16);
		State myMenu = new State(this, new Vec2D(), 200, 45);
		State myItems = new State(this, new Vec2D(), 200, 30);
		myList = new SListHandler(myMenu, myItems, pf, "My Title");
	}

	public void draw() {
		background(255);
		myList.arrange(myList.getPos(), 30, height);
		myList.run();
	}

}
