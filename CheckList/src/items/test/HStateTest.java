package items.test;

import items.gui.CheckBox;
import items.gui.HState;
import items.gui.State;
import items.gui.Text;
import processing.core.PApplet;
import processing.core.PFont;
import toxi.geom.Vec2D;

public class HStateTest extends PApplet {

//	HState myStates;
	PFont pf;
	Text myTextBox2;

	public void setup() {
		size(500, 500);
		pf = createFont("Times New Roman Bold Italic", 15);

//		myStates = new HState(new State(this, new Vec2D(50, 50), 100, 30));

		CheckBox myBox = new CheckBox(new State(this, new Vec2D(0, 0), 30, 30));
		myBox.setID("CheckBox");
		CheckBox myBox2 = new CheckBox(new State(this, new Vec2D(0, 0), 30, 30));
		myBox2.setID("CheckBox2");

		Text myTextBox = new Text(new State(this, new Vec2D(), 150, 30), pf,
				"text");
		State myState = new State(this, new Vec2D(), 150, 30);
		
		myTextBox2 = new Text(myState, pf,
				"text");
		
		myTextBox.setAdjustable(true);

//		myStates.add(myBox);
//		myStates.add(myBox2);
//		myStates.add(myTextBox);
	}

	public void draw() {
		background(255);
//		myStates.move(new Vec2D(mouseX, mouseY));
//		myStates.setSizeX(200);
//		myStates.get(2).update();
//		myStates.run();
		myTextBox2.run();
	}

}
