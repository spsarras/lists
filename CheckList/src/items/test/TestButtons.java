package items.test;

import items.gui.Button;
import items.gui.CheckBox;
import items.gui.State;
import processing.core.PApplet;
import toxi.geom.Vec2D;

public class TestButtons extends PApplet {

	CheckBox myCheckBox;
	Button myButton;

	public void setup() {
		size(240, 80);
		smooth();
		myCheckBox = new CheckBox(new State(this, new Vec2D(0, 40), 40, 40));
		myButton = new Button(new State(this, new Vec2D(20, 0), 40, 40));
	}

	public void draw() {
		background(0);
		myCheckBox.draw();
		myCheckBox.update();

		myButton.move(new Vec2D(20, 0));
		
		for (int i = 0; i < 6; i++) {
			myButton.move(new Vec2D(40 * i, 0));

			switch (i) {
			case 0:
				myButton.drawExport();
				break;
			case 1:
				myButton.drawCircle(false);
				break;
			case 2:
				myButton.drawDisk(false);
				break;
			case 3:
				myButton.drawPlus(false);
				break;
			case 4:
				myButton.drawDetail();
				break;
			case 5:
				myButton.drawNext();
				break;
				
				
			}

		}

	}
}
