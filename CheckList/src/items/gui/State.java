package items.gui;

import toxi.geom.Vec2D;
import processing.core.PApplet;
import processing.core.PConstants;
import processing.event.MouseEvent;

//TODO
//Maybe interact the Border for mouseOver? nice!

/**
 * @author Stamatis This class handles the Click Event for Interacting with the
 *         UI elements Also has some basic parameters like the @pos and the
 * @sizeX , @sizeY of the element that every class needs.
 */
public class State {

	private String ID = "00";
	private transient Vec2D pos;
	private float sizeX = 0;
	private float sizeY = 0;
	private boolean adjustable = false;
	
	private boolean LeftClick = false;
	
	protected PApplet pa;

	/**
	 * @param pos
	 * @param sizeX
	 * @param sizeY
	 *            Basic Initialisation, using the PApplet to register the
	 *            mouseEvent and therefore interact with the mouse!
	 */
	public State(PApplet pa, Vec2D pos, float sizeX, float sizeY) {
		this.pos = pos;
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.pa = pa;
		ID = String.valueOf((int) (pa.random(1, 2) * 1000f));
		pa.registerMethod("mouseEvent", this);
	}

	public State(State state) {
		this.ID = state.getID();
		this.pos = state.getPos().copy();
		this.sizeX = state.getSizeX();
		this.sizeY = state.getSizeY();
		this.pa = state.getPA();
		pa.registerMethod("mouseEvent", this);
	}
	
	public PApplet getPA() {
		return pa;
	}

	/**
	 * @param pos
	 *            Move the Element to a new Position, note the size doesn't
	 *            change!
	 */
	public void move(Vec2D pos) {
		this.pos = pos;
	}

	/**
	 * @param sizeX
	 * @param sizeY
	 *            Change Size of the Y and X direction. This also changes the
	 *            range of the Interaction, increasing the are that the Class
	 *            interacts!
	 */
	public void resize(float sizeX, float sizeY) {
		this.sizeX = sizeX;
		this.sizeY = sizeY;
	}

	/**
	 * @param pa
	 *            Draws a Bounding Box around the State element
	 */
	public void drawBB() {
		pa.pushStyle();
		pa.noFill();
		// pa.fill(50, 50);
		pa.stroke(150);
		pa.rect(pos.x, pos.y, sizeX, sizeY);
		pa.popStyle();
	}

	/**
	 * Implementation of the mouseEvent method
	 * 
	 * @param e
	 *            you can get all sort of nice action from the MouseEvent class!
	 *            Here @leftClick becomes true every time the Left mouseButton
	 *            is clicked
	 */
	public void mouseEvent(MouseEvent e) {
		switch (e.getAction()) {
		case MouseEvent.CLICK:
			if (e.getButton() == PConstants.LEFT)
				LeftClick = true;
			break;
		}
	}

	/**
	 * Resets the status of the LeftClick so that you can use onClick more than
	 * once!
	 */
	public void reset() {
		LeftClick = false;
	}

	/**
	 * Base Method to determine the Interactions Resets @LeftClick immediately
	 * to False!
	 * 
	 * @param pa
	 * @return Return True or False if there is a Click on top of the Element
	 */
	public boolean onClick() {
		if (LeftClick) {
			// LeftClick = false;
			if (mouseOver()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Base Method to determine if the Mouse is Over the Element, used on
	 * 
	 * @onClick()
	 * 
	 * @return
	 */
	public boolean mouseOver() {
		if (pa.mouseX < pa.screenX(pos.x + sizeX, 0)
				&& pa.mouseX > pa.screenX(pos.x, 0)) {
			if (pa.mouseY < pa.screenY(0, pos.y + sizeY)
					&& pa.mouseY > pa.screenY(0, pos.y)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Draw a Border Around the Element When the mouse is over it!
	 */
	public void highlight() {
		if (mouseOver()) {
			drawBB();
		}
	}

	// Standard Getters/Setters

	public String getID() {
		return ID;
	}

	public void setID(String id) {
		this.ID = id;
	}

	public Vec2D getPos() {
		return this.pos;
	}

	public void setSizeX(float value) {
		this.sizeX = value;
	}

	public float getSizeX() {
		return this.sizeX;
	}

	public void setSizeY(float value) {
		this.sizeY = value;
	}

	public float getSizeY() {
		return this.sizeY;
	}

	public void update() {
		// TODO Auto-generated method stub

	}

	public void draw() {

	}

	public void setAdjustable(boolean toAdjust) {
		adjustable = toAdjust;
	}

	public boolean isAdjustable() {
		return adjustable;
	}

	public void setPos(Vec2D pos) {
		this.pos = pos;
	}

	public State copy() {
		State myState = new State(this);
		return myState;
	}

}
