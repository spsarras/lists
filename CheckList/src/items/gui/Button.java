package items.gui;

import items.util.ColorHandler;
import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PImage;
import toxi.geom.Vec2D;

public class Button extends State {

	ColorHandler myColors;

	public Button(PApplet pa, Vec2D pos, float size) {
		super(pa, pos, size, size);
		myColors = new ColorHandler();
	}

	public Button(State state) {
		super(state);
		myColors = new ColorHandler();
	}

	/**
	 * A HardCoded Right Arrow
	 */
	public void drawNext() {
		float nudge = 3;
		pa.pushStyle();
		pa.fill(myColors.getColorBackground());
		pa.stroke(myColors.getColorOutlineS());
		
		pa.rect(getPos().x, getPos().y, getSizeX(), getSizeY(), 3);

		pa.fill(myColors.getColorBackground2());
		
		pa.rect(getPos().x + nudge, getPos().y + nudge, getSizeX() - 2 * nudge,
				getSizeY() - 2 * nudge, 3);

		pa.stroke(myColors.getColorOutlineH());
		pa.fill(myColors.getColorContent());
		pa.stroke(myColors.getColorOutlineH());
		
		pa.beginShape();
		pa.vertex(getPos().x + 2 * nudge, getPos().y + nudge * 1.5f);
		pa.vertex(getPos().x + getSizeX() - 2 * nudge, getPos().y + getSizeY()
				/ 2.0f);
		pa.vertex(getPos().x + 2 * nudge, getPos().y + getSizeY() - nudge
				* 1.5f);
		pa.endShape(PConstants.CLOSE);
		pa.popStyle();
	}

	/**
	 * A HardCoded Button 'Export Facing top Right Arrow'
	 */
	public void drawExport() {
		float nudge = 3;
		pa.pushStyle();
		pa.stroke(100);
		pa.fill(220);
		pa.rect(getPos().x, getPos().y, getSizeX(), getSizeY(), 3);
		pa.fill(35, 125, 172, 50);
		pa.rect(getPos().x + nudge, getPos().y + nudge, getSizeX() - 2 * nudge,
				getSizeY() - 2 * nudge, 3);

		pa.stroke(50);
		pa.strokeWeight(3);
		pa.line(getPos().x + getSizeX() / 3.0f, getPos().y + getSizeY() / 10.0f
				+ nudge, getPos().x + getSizeX() - 2 * nudge, getPos().y
				+ getSizeY() / 10.0f + nudge);
		pa.line(getPos().x + getSizeX() - 2 * nudge, getPos().y + 2
				* getSizeY() / 3.0f, getPos().x + getSizeX() - 2 * nudge,
				getPos().y + getSizeY() / 10.0f + nudge);
		pa.line(getPos().x + getSizeX() / 3.0f, getPos().y + getSizeY()
				- getSizeX() / 3.0f, getPos().x + getSizeX() - 2 * nudge,
				getPos().y + getSizeY() / 10.0f + nudge);
		pa.popStyle();
	}

	/**
	 * A HardCoded Button 'Notes on Paper'
	 */
	public void drawDetail() {
		float nudge = 3;
		pa.pushStyle();
		pa.stroke(100);
		pa.fill(220);
		pa.rect(getPos().x, getPos().y, getSizeX(), getSizeY(), 3);
		pa.fill(150);
		pa.rect(getPos().x + nudge, getPos().y + nudge, getSizeX() - 2 * nudge,
				getSizeY() - 2 * nudge, 3);
		pa.fill(200);
		for (int i = 2; i < 6; i++) {
			pa.strokeWeight(3);
			pa.point(getPos().x + nudge * 3, getPos().y + i * (getSizeY() / 7));
			pa.strokeWeight(1);
			pa.line(getPos().x + nudge * 4, getPos().y + i * (getSizeY() / 7),
					getPos().x + getSizeX() - nudge * 3, getPos().y + i
							* (getSizeY() / 7));
		}
		pa.popStyle();
	}

	/**
	 * A HardCoded DiskButton 'Old Disquette signifying the Save Button'
	 * 
	 * @param type
	 */
	public void drawDisk(boolean type) {
		float nudge = 3;
		pa.pushStyle();
		pa.stroke(100);
		pa.fill(220);
		pa.rect(getPos().x, getPos().y, getSizeX(), getSizeY(), 3);
		if (type)
			pa.fill(35, 125, 172, 50);
		else
			pa.fill(142, 12, 12);
		pa.rect(getPos().x + nudge, getPos().y + nudge, getSizeX() - 2 * nudge,
				getSizeY() - 2 * nudge, 3);

		pa.fill(200);
		pa.rect(getPos().x + getSizeX() / 3.0f, getPos().y + nudge, 2
				* (getSizeX() / 3.0f) - 2 * nudge, getSizeY() / 3.0f);
		pa.fill(100);
		pa.rect(getPos().x + 2 * getSizeX() / 3.0f, getPos().y + nudge,
				getSizeX() / 5.0f - nudge, getSizeY() / 5.0f);
		pa.popStyle();
	}

	/**
	 * A HardCoded Plus Button 'Simple Cross' to add new Items
	 * 
	 * @param type
	 */
	public void drawPlus(boolean type) {
		float nudge = 3;
		pa.pushStyle();
		pa.fill(220);
		pa.stroke(100);
		pa.strokeWeight(1f);
		pa.rect(getPos().x, getPos().y, getSizeX(), getSizeY(), 3);
		if (type) {
			pa.fill(35, 125, 172, 50);
			pa.rect(getPos().x + nudge, getPos().y + nudge, getSizeX() - 2
					* nudge, getSizeY() - 2 * nudge, 3);
			pa.strokeWeight(2);
			pa.stroke(50);
			pa.line(getPos().x + getSizeX() / 2.0f, getPos().y + 2 * nudge,
					getPos().x + getSizeX() / 2.0f, getPos().y + getSizeY() - 2
							* nudge);
			pa.line(getPos().x + 2 * nudge, getPos().y + getSizeY() / 2.0f,
					getPos().x + getSizeX() - 2 * nudge, getPos().y
							+ getSizeY() / 2.0f);
		} else {
			pa.stroke(0);
			pa.strokeWeight(1.0f);
			pa.line(getPos().x + getSizeX() / 2.0f, getPos().y + nudge,
					getPos().x + getSizeX() / 2.0f, getPos().y + getSizeY()
							- nudge);
			pa.line(getPos().x + nudge, getPos().y + getSizeY() / 2.0f,
					getPos().x + getSizeX() - nudge, getPos().y + getSizeY()
							/ 2.0f);
		}
		// noFill();
		pa.popStyle();
	}

	/**
	 * A HardCoded Circle with an X, signifying delete
	 * 
	 * @param small
	 */
	public void drawCircle(boolean small) {
		pa.ellipseMode(PConstants.CORNER);
		float nudge;
		if (small) {
			nudge = 3;
		} else {
			nudge = 4.5f;
		}
		pa.pushStyle();
		pa.strokeWeight(1.0f);
		pa.fill(150);
		pa.noStroke();
		pa.ellipse(getPos().x, getPos().y, getSizeX(), getSizeY());
		pa.stroke(255);
		pa.line(getPos().x + nudge, getPos().y + nudge, getPos().x + getSizeX()
				- nudge, getPos().y + getSizeY() - nudge);
		pa.line(getPos().x + nudge, getPos().y + getSizeY() - nudge, getPos().x
				+ getSizeX() - nudge, getPos().y + nudge);
		pa.popStyle();
	}

	/**
	 * A HardCoded Plus Button 'Simple Cross' to add new Items
	 * 
	 * @param type
	 */
	public void drawImage(PImage img) {
		float nudge = 3;
		pa.pushStyle();
		pa.fill(220);
		pa.stroke(100);
		pa.strokeWeight(1.2f);
		pa.rect(getPos().x, getPos().y, getSizeX(), getSizeY(), 3);
		pa.fill(35, 125, 172, 50);
		pa.rect(getPos().x + nudge, getPos().y + nudge, getSizeX() - 2 * nudge,
				getSizeY() - 2 * nudge, 3);
		nudge = 5;
		pa.image(img, getPos().x + nudge, getPos().y + nudge, getSizeX() - 2
				* nudge, getSizeY() - 2 * nudge);
		// noFill();
		pa.popStyle();
	}

	public void update() {
		// TODO Auto-generated method stub
	}

}
