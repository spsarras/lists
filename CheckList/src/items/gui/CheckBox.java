package items.gui;

import processing.core.PApplet;
import toxi.geom.Vec2D;

public class CheckBox extends State {
	private boolean check;
	private float nudge = 2;

	public CheckBox(State state) {
		super(state);
	}
	
	public CheckBox(PApplet pa, Vec2D pos, float size) {
		super(pa, pos, size, size);
	}

	/**
	 * Draws a CheckBox with an 'x' if @check is true or without if false
	 */
	public void draw() {
		pa.pushStyle();
		pa.fill(200);
		pa.stroke(0);
		pa.strokeWeight(1.2f);
		pa.rect(getPos().x, getPos().y, getSizeX(), getSizeY(), 3);
		pa.stroke(255, 50, 50);
 
		if (isChecked()) {
			pa.line(getPos().x + nudge, getPos().y + nudge, getPos().x
					+ getSizeX() - nudge, getPos().y + getSizeY() - nudge);
			pa.line(getPos().x + nudge, getPos().y + getSizeY() - nudge,
					getPos().x + getSizeX() - nudge, getPos().y + nudge);
		}
		pa.popStyle();
	}

	/**
	 * While this is updated the the @CheckBox will react to mouseClicks
	 */
	public void update() {
		if (this.onClick()) {
			check = !check;
		}
	}

	public void check() {
		check = true;
	}

	public void uncheck() {
		check = false;
	}

	public boolean isChecked() {
		return check;
	}

	// Setter / Getter

	public void setNudge(float value) {
		this.nudge = value;
	}

	public float getNudge() {
		return this.nudge;
	}

}
