package items.gui;

import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PFont;
import processing.event.KeyEvent;
import toxi.geom.Vec2D;

public class Text extends State  {

	private String txt;
	private transient PFont txtFont;
	// private Boolean focus = false;

	private boolean toEdit = false;

	public Text(State state, PFont txtFont, String txt) {
		super(state);
		this.txtFont = txtFont;
		this.txt = txt;
		pa.registerMethod("keyEvent", this);		
	}
	
	public Text(PApplet pa, Vec2D pos, float sizeX,
			float sizeY, PFont txtFont, String txt) {
		super(pa, pos, sizeX, sizeY);
		this.txtFont = txtFont;
		this.txt = txt;
		pa.registerMethod("keyEvent", this);
	}

	public void keyEvent(KeyEvent e) {
		if (isToEdit() && e.getAction() == KeyEvent.RELEASE) {
			if (e.getKeyCode() == 8)
				this.deleteLast();
			else if (e.getKeyCode() == 10)
				setToEdit(false);
			else
				this.addChar(e.getKey());
		}
	}

	public void setTxtFont(PFont value) {
		this.txtFont = value;
	}

	public PFont getTxtFont() {
		return this.txtFont;
	}

	/**
	 * Method to contain all the functionality
	 */
	public void update() {
		if (this.onClick())
			toEdit = true;
	}

	/**
	 * Draw the Text
	 */
	public void draw() {
		if (isToEdit())
			drawFocus();

		float nudge = 3;
		pa.pushStyle();
		pa.fill(0);
		pa.textAlign(PConstants.LEFT, PConstants.CENTER);
		pa.textFont(txtFont);
		pa.text(txt, getPos().x + nudge, getPos().y, getSizeX(), getSizeY());
		pa.popStyle();
		highlight();
	}

	/**
	 * Draw a Grey box with soften edges behind the Text
	 */
	public void drawFocus() {
		float nudge = 2;
		pa.pushStyle();
		pa.fill(150);
		pa.rect(getPos().x + nudge, getPos().y + nudge, getSizeX() - 2 * nudge,
				getSizeY() - 2 * nudge, 3);
		pa.popStyle();
	}

	/**
	 * @param name
	 */
	public void seTXT(String txt) {
		this.txt = txt;
	}

	public String getTXT() {
		return this.txt;
	}

	public void addChar(char c) {
		pa.textSize(txtFont.getSize());
		if (pa.textWidth(txt) < getSizeX() - pa.textWidth(c) - 5) // make this
																	// in
																	// reference
																	// to the
																	// textSize!
			txt += c;
	}

	/**
	 * DeleteLast Character
	 */
	public void deleteLast() {
		if (txt.length() > 0) {
			txt = txt.substring(0, txt.length() - 1);
		}
	}

	public boolean isToEdit() {
		return toEdit;
	}

	public void setToEdit(boolean toEdit) {
		this.toEdit = toEdit;
	}

	public void run() {
		update();
		draw();
		reset();		
	}

}
