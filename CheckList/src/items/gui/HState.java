package items.gui;

import java.util.ArrayList;

import processing.core.PApplet;
import toxi.geom.Vec2D;

/**
 * 
 * Class to create Horizontal stripes of GUI
 * 
 * @author Stamatis
 *
 */
public class HState extends State {

	ArrayList<State> items;

	public HState(PApplet pa, Vec2D pos, float sizeX, float sizeY) {
		super(pa, pos, sizeX, sizeY);
		items = new ArrayList<State>();
	}

	public HState(State state) {
		super(state);
		items = new ArrayList<State>();
	}

	/**
	 * Check if GUI items are longer than the allowed sizeX Return how much
	 * larger it is, negative values indicate that the GUI elements are smaller
	 * 
	 * @return
	 */
	public float isLarger() {
		float size = 0;
		for (int i = 0; i < size(); i++) {
			size += get(i).getSizeX();
			// System.out.println(size);
		}
		return size - getSizeX();
	}

	public ArrayList<Integer> whoAdjustable() {

		ArrayList<Integer> indexes = new ArrayList<Integer>();

		for (int i = 0; i < size(); i++) {
			if (get(i).isAdjustable())
				indexes.add(i);
		}

		return indexes;
	}

	@Override
	public void move(Vec2D newPos) {
		super.move(newPos);
		// adjust();
		// arrayH();
		// alignCentreV();
	}

	public void adjust() {
		float dif = isLarger();
		// System.out.println(dif);
		// if(dif > 0) {
		ArrayList<Integer> indexes = whoAdjustable();
		if (indexes.size() > 0) {
			float adjustment = dif / indexes.size();
			for (int i = 0; i < indexes.size(); i++) {
				State myState = get(indexes.get(i));
				float originalSize = myState.getSizeX();
				myState.setSizeX(originalSize - adjustment);
			}
		}
		// }
	}

	/**
	 * Sort the Items horizontally
	 */
	public void arrayH() {
		for (int i = 0; i < size(); i++) {
			Vec2D newPos = getNextPos(i);
			get(i).move(newPos);
			// System.out.println(newPos);
		}
	}

	/**
	 * Get the next position for the next Item
	 * 
	 * @param index
	 * @return
	 */
	private Vec2D getNextPos(int index) {
		if (index > 0 && index < size()) {
			State myState = get(index - 1);
			Vec2D newPos = myState.getPos();
			newPos.y = getPos().y;
			return newPos.add(myState.getSizeX(), 0);
		} else {
			return getPos();
		}
	}

	/**
	 * Get Next place holder for the Item
	 * 
	 * @return
	 */
	public Vec2D getNextPos() {
		return getNextPos(size() - 1);
	}

	public void add(State item) {
		items.add(item);
		arrayH();
	}

	public void draw() {
		super.drawBB();
		for (int i = 0; i < size(); i++) {
			get(i).draw();
		}
	}

	public void update() {
		adjust();
		arrayH();
		super.update();
		for (int i = 0; i < size(); i++) {
			get(i).update();
		}
	}

	public void reset() {
		super.reset();
		for (int i = 0; i < size(); i++) {
			get(i).reset();
		}
	}

	public void run() {
		update();
		draw();
		reset();
	}

	public void alignCentreV() {
		for (int i = 0; i < size(); i++) {
			get(i).getPos().y += (getSizeY() - get(i).getSizeY()) / 2f;
		}
	}

	public void alignCentreH() {

	}

	/**
	 * Get the Item by comparing the ID
	 * 
	 * @param name
	 * @return
	 */
	public State get(String name) {
		for (int i = 0; i < size(); i++) {
			String id = items.get(i).getID();
			if (id.equals(name))
				return items.get(i);
		}
		return null;
	}

	/**
	 * Get the Item using an index
	 * 
	 * @param index
	 * @return
	 */
	public State get(int index) {
		if (index >= 0 && index < size())
			return items.get(index);
		else
			return null;
	}

	/**
	 * Get the size of the collection
	 * 
	 * @return
	 */
	public int size() {
		return items.size();
	}

	public void remove(String name) {
		int index = -1;
		for (int i = 0; i < size(); i++) {
			String id = items.get(i).getID();
			if (id.equals(name)){
				index = i;
//				System.out.println(i);
			}
		}		
		items.remove(index);
	}
}
