package items.util;

import toxi.color.ColorList;
import toxi.color.TColor;

public class ColorHandler {

//	public AnalogousStrategy myStrategy;
//	public ComplementaryStrategy myNewStrategy;
	
	ColorList myColors;

	public ColorHandler() {
//		myStrategy = new AnalogousStrategy();
//		myStrategy.createListFromColor(TColor.newRandom());
//		myNewStrategy = new ComplementaryStrategy();
		//toxi.color.ColorRange;	
		
		myColors = new ColorList();
		
//		myColors.add(TColor.newRandom());	//Background 1
		myColors.add(TColor.newRGB(220/255f, 220/255f, 220/255f)); //OutlineS
//		myColors.add(TColor.newRGB(148/255f,177/255f,224/255f)); //OutlineS
//		myColors.add(TColor.newRGB(0/255f,179/255f,255/255f)); //OutlineS
//		System.out.println(myColors.get(0));
//		myColors.add(TColor.newRGB(51/255f,51/255f,51/255f));	//OutlineH
//		myColors.add(TColor.newRGB(0, 0, 0));		//Content
		
		myColors.add(myColors.get(0).copy().lighten(.2f));
		myColors.add(getAnalog());
		myColors.add(getAnalog().darken(.2f));
		myColors.add(myColors.get(0).copy().complement());
		
	}
	
	
	public int getColorBackground() {
		return myColors.get(0).toARGB();
	}

	public int getColorBackground2() {
		return myColors.get(1).toARGB();
	}
	
	private TColor getAnalog() {
		return myColors.get(0).copy().analog(1.4f, 0.3f);
	}

	public int getColorOutlineS() {
		return myColors.get(2).toARGB();
	}
	
	public int getColorOutlineH() {
		return myColors.get(3).toARGB();		
	}
	
	//It is the Complementary of Background so there will be contrast
	public int getColorContent() {
		return myColors.get(4).toARGB();		
	}
	
}
