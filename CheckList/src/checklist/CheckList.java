package checklist;

import items.gui.State;
import items.list.SCartManagerHandler;
import processing.core.PApplet;
import processing.core.PFont;
import toxi.geom.Vec2D;

public class CheckList extends PApplet {

	private PFont txtFont;
	SCartManagerHandler myProgram;

	public void setup() {
		size(1200, 720);
		smooth(4);

		txtFont = createFont("MyriadPro-Regular", 16);
//		System.out.println(Arrays.toString(PFont.list()));
		State menu = new State(this, new Vec2D(220, 0), width - 220, 40);
		State item = new State(this, new Vec2D(0, 0), 220, 27.7f);
		
		myProgram = new SCartManagerHandler(menu, item, txtFont);

	}

	public void draw() {
		background(255);

		myProgram.run();
	}
	
	public static void main(String[] args) {
		PApplet.main(new String[] { checklist.CheckList.class.getName() });
	}

}
